# Libs role
## Actions
* **file** - create file from inventory
* **files** - create multiple files from inventory
* **file_raw** - create file from inventory raw format
* **files_raw** - create multiple files from inventory raw format
* **files_64** - create multiple files from inventory base64 format
* **sshkey** - create (if not exist) and return pub keyfile
* **fetch_file** - fetch files from one dir
* **fetch_files** - fetch files from multiple dirs

## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 and python-apt on remote server.
```bash
ansible xen -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt;"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download libs role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/libs.git
  name: libs
EOF
echo "roles/libs" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## Libs
Run libs actions.
```bash
cat << 'EOF' > libs.yml
---
- hosts:
    libs
  tasks:
    - name: Libs create file from inventory
      include_role:
        name: libs
      vars:
        libs_action: file
        file:
          dest: "/tmp/somefile"
          owner: 'root'
          group: 'root'
          mode: '0644'
          backup: 'no'
          content:
            - 'string 0'
            - 'string 0'

    - name: Libs create multiple files from inventory
      include_role:
        name: libs
      vars:
        libs_action: files
        files:
          - dest: "/tmp/somefile1"
            owner: 'root'
            group: 'root'
            mode: '0644'
            backup: 'no'
            content:
              - 'string 1'
              - 'string 2'
          - dest: "/tmp/somefile2"
            owner: 'root'
            group: 'root'
            mode: '0644'
            backup: 'no'
            content:
              - 'string 12'
              - 'string 22'

    - name: Libs create file from inventory in raw format
      include_role:
        name: libs
      vars:
        libs_action: file_raw
        file:
          dest: "/tmp/somefile"
          owner: 'root'
          group: 'root'
          mode: '0644'
          backup: 'no'
          content: |
            string 0
            string 1
            string 2

    - name: Libs create multiple files from inventory in raw format
      include_role:
        name: libs
      vars:
        libs_action: files_raw
        files:
          - dest: "/tmp/somefile1"
            owner: 'root'
            group: 'root'
            mode: '0644'
            backup: 'no'
            content: |
              string 1
              string 2
          - dest: "/tmp/somefile2"
            owner: 'root'
            group: 'root'
            mode: '0644'
            backup: 'no'
            content: |
              string 12
              string 22

    - name: Libs create multiple files from inventory in base64 file format
      include_role:
        name: libs
      vars:
        libs_action: files_64
        files:
          - dest: "/tmp/somefile.base64"
            real: "/tmp/somefile"
            owner: 'root'
            group: 'root'
            mode: '0644'
            backup: 'no'
            content: |
              c3RyaW5nIDEKc3RyaW5nIDIK
          - dest: "/tmp/somefile2.base64"
            real: "/tmp/somefile2"
            owner: 'root'
            group: 'root'
            mode: '0644'
            backup: 'no'
            content: |
              c3RyaW5nIDEyCnN0cmluZyAyMgo=

    - name: Libs sshkey
      include_role:
        name: libs
      vars:
       libs_action: sshkey
       ssh_user:
         name: 'postgres'
         command: 'ssh-keygen -q -t ed25519 -a 100 -N "" -C "${USER}@$(hostname -A)"'
         file: 'id_ed25519'

    - name: Libs fetch files from one dir
      include_role:
        name: libs
      vars:
        libs_action: 'fetch_file'
        fetch_file:
          find: 'find /etc/rsyslog.d/ -type f'
          dest: "{{ playbook_dir }}/get_rsyslog/{{ ansible_fqdn }}/rsyslog.d/"

    - name: Lib fetch files from multiple dirs
      include_role:
        name: libs
      vars:
        libs_action: 'fetch_files'
        fetch_files:
          - find: 'find /etc/rsyslog.d/ -type f'
            dest: "{{ playbook_dir }}/get_rsyslog/{{ ansible_fqdn }}/rsyslog.d/"

          - find: 'find /etc/rsyslog.conf -type f'
            dest: "{{ playbook_dir }}/get_rsyslog/{{ ansible_fqdn }}/"

          - find: 'find /etc/apt/sources.list.d/ -type f'
            dest: "{{ playbook_dir }}/get_apt/{{ ansible_fqdn }}/sources.list.d/"

          - find: 'find /etc/apt/apt.conf.d/ -type f'
            dest: "{{ playbook_dir }}/get_apt/{{ ansible_fqdn }}/apt.conf.d/"

          - find: 'find /etc/apt/sources.list -type f'
            dest: "{{ playbook_dir }}/get_apt/{{ ansible_fqdn }}/"

          - find: 'find /etc/logrotate.d/ -type f'
            dest: "{{ playbook_dir }}/get_logrotate/{{ ansible_fqdn }}/get_logrotate/"

          - find: 'find /etc/hosts -type f'
            dest: "{{ playbook_dir }}/get_hosts/{{ ansible_fqdn }}/"

          - find: 'find /etc/resolv.conf -type f'
            dest: "{{ playbook_dir }}/get_resolv/{{ ansible_fqdn }}/"

          - find: 'find /etc/ntp.conf -type f'
            dest: "{{ playbook_dir }}/get_ntp/{{ ansible_fqdn }}/"

          - find: 'find /var/spool/cron/crontabs/ -type f'
            dest: "{{ playbook_dir }}/get_crontabs/{{ ansible_fqdn }}/"

EOF

ansible-playbook ./libs.yml
```
